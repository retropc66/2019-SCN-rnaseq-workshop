---
title: 'RNA-seq workshop - human satellite cells'
author: "Christopher Porter, Ottawa Bioinformatics Core Facility (OBCF)"
date: "`r format(Sys.Date(),'%B %d, %Y')`"
output: html_notebook
---

```{r initialize,results="hide",message=F,echo=F,warning=F}

## Load the libraries required for the analysis

library("DESeq2")           # Fold change analysis
library("gplots")           # Plotting functions
library("RColorBrewer")     # Colour palettes
library("ggplot2")          # Plotting functions
library("biomaRt")          # Download gene annotation
library("tximport")         # Importing transcript counts
library("apeglm")           # Method for shrinking fold changes
library("jsonlite")         # Reading structured text files (JSON format)

``` 


Useful links describing RNA-seq analysis with DESeq2:

* https://bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html
* https://www.bioconductor.org/packages/devel/workflows/vignettes/rnaseqGene/inst/doc/rnaseqGene.html

This document contains an analysis of six RNA-seq libraries downloaded from Ensembl (https://www.ebi.ac.uk/ena/data/view/PRJEB10091).

* Quiescent satellite cells
* Activated satellite cells
* Activated satellite cells treated with P38 inhibitor

Reads were aligned with salmon

* https://combine-lab.github.io/salmon/
* https://salmon.readthedocs.io/en/latest/


First check stats for alignment - look at an example first, then load in R to summarize

```{r}

salmon_dir<-"~/RNA-seq_analysis_course/salmon/"
samples<-c("Activated.1","Activated.2","Activated_P38i.1","Activated_P38i.2","Quiescent.1","Quiescent.2")
sampleNames<-samples

salmon_meta<-paste0(salmon_dir,samples,"/aux_info/meta_info.json")
assignment_sum<-matrix(ncol=3,nrow=0)    

for(s in 1:length(samples)){
    sumtmp<-fromJSON(salmon_meta[s])
    assignment_sum<-rbind(assignment_sum,c(sumtmp$num_processed,sumtmp$num_mapped,sumtmp$percent_mapped))
}
rownames(assignment_sum)<-sampleNames
colnames(assignment_sum)<-c("Reads processed","Reads mapped","Percent mapped")

View(assignment_sum)

```

Assignment rates are lower for quiescent SC samples. This is in large part because of a higher fraction of intronic reads in both quiescent samples (up to 45% in one sample, vs ~10% in the activated samples). 

See RNA-SeQC reports:

* http://www.ogic.ca/projects/workshop_2019/RNA-SeQC/Activated.1/
* http://www.ogic.ca/projects/workshop_2019/RNA-SeQC/Quiescent.1/


## Run DESeq2

* Import transcript abundances from salmon using the `tximport` library.
* Filter to retain only genes with 5 or more counts in two or more samples.

Salmon assigns reads to transcripts, but we want to run DESeq2 on genes. When importing transcript counts, the `tximport` library aggregates transcript counts into gene counts using a `tx2gene` table.

```{r read_Tx2gene}
    gtfdir<-"~/RNA-seq_analysis_course/gtf/"
    gtffile<-paste0(gtfdir,"gencode.v29.annotation.gtf")
    tx2gene_file<-paste0(gtfdir,"tx2gene.GENCODE.v29.txt")
    if(!file_test("-f",tx2gene_file)){  ## If the tx2gene file isn't where we said them make a new one
        message("Making TxDB")
        txdb<-makeTxDbFromGFF(gtffile)  ## Reads the GTF file and makes a TxDb object
        k<-keys(txdb,keytype="TXNAME")
        tx2gene<-select(txdb,k,"GENEID","TXNAME")
        write.table(tx2gene,file=tx2gene_file,sep="\t",row.names=F,col.names=T,quote=F)
    } else {
        message("Reading tx2gene")
        tx2gene<-read.table(tx2gene_file,sep="\t",header=T,stringsAsFactors=F)        
    }
```


Import transcripts (convert to gene level)

```{r import_transcripts}
    files<-paste(salmon_dir,samples,"quant.sf",sep="/")
    txi<-tximport(files,type="salmon",tx2gene=tx2gene)

    View(txi)
    View(txi$counts)
    nrow(txi$counts)

    # How many rows (genes) have zero counts across all samples?
    sum(rowSums(txi$counts)==0)
    
    # How many have fewer than 10 reads?
    sum(rowSums(txi$counts)<10)
    
    # Create a filter (vector) showing which rows have at least two columns with 5 or more counts
    txi.filter<-apply(txi$counts,1,function(x) length(x[x>5])>=2)
    head(txi.filter)
    sum(txi.filter)
    txi$counts<-txi$counts[txi.filter,]
    txi$abundance<-txi$abundance[txi.filter,]
    txi$length<-txi$length[txi.filter,]
```

* Annotate samples
* Run DESeq2
    + estimate size factors
    + estimate dispersions
    + estimate gene-wise dispersions
    + calculate mean-dispersion relationship
    + calculate final dispersion estimates
    + fit model and test
* Experimental design is `~condition`
* Calculate rlog-transformed normalized counts for PCA and clustering.

```{r run_DESeq2,cache=T,warning=F,message=F,eval=T}
## Add sample annotations

condition<-rep(c("Act","P38i","Qui"),each=2)
sampleTable<-data.frame(sampleName=sampleNames,condition=condition)

## Generate DESeq data set
dds<-DESeqDataSetFromTximport(txi,colData=sampleTable,design=~condition)
colData(dds)$condition<-factor(colData(dds)$condition,
                               levels=c("Qui","Act","P38i"))

## Run DESeq
# dds<-DESeq(dds,parallel=T,BPPARAM=MulticoreParam(4))
dds<-DESeq(dds)

View(colData(dds))

```

### Diagnostic plots

The following plots show the relationship between the replicates, based on log-transformed normalized read counts. 

#### Principal Component Analysis (PCA)

Applying principal component analysis (PCA) to the matrix of gene expression values (read counts) identifies the major components of gene expression variation. Plotting the principal component values for each sample gives an overview of the relationship between the samples. The implementation in DESeq by default uses the 500 most variable genes in the dataset to generate the PCA. 

```{r plot_PCA,fig.width=8,fig.height=8,eval=T,echo=F}

# Make rlog (regularlized log) transform of counts

rld<-rlog(dds)

# Plot PCA, and add text annotation of sample names
plotPCA(rld,intgroup="condition")+geom_text(aes(label=colData(dds)$sampleName),
                                        hjust=0,vjust=1,size=4)
```



#### Hierarchical clustering

Hierarchical clustering is calculated using Euclidian distance between rlog-transformed normalized count values for all transcripts.

```{r plot_hierarchical_cluster,fig.width=6,fig.height=6,eval=T}

## Set up a colour ramp palette 
colours<-colorRampPalette(rev(brewer.pal(9,'Blues')))(255)

## Calculate the Euclidian distance between samples, based on read counts per gene
sampleDists<-dist(t(assay(rld)))
sdm<-as.matrix(sampleDists)
rownames(sdm)<-colData(rld)$sampleName
colnames(sdm)<-colData(rld)$sampleName

## Plot heatmap of sample distances
heatmap.2(sdm,trace="none",col=colours,cexRow=0.8,cexCol=0.8)
```



Get annotation for genes from Ensembl

```{r getEnsembl,cache=T,eval=T}
## Fetch annotation data from Ensembl
ensembl = useEnsembl( "ensembl", dataset="hsapiens_gene_ensembl",mirror="useast")
genemap<-getBM(attributes=c("ensembl_gene_id","hgnc_symbol","entrezgene","gene_biotype"),
               filters="ensembl_gene_id",
               values=sapply(strsplit(rownames(dds),"[.]"),"[",1),mart=ensembl)

genemap_idx<-match(sapply(strsplit(rownames(dds),"[.]"),"[",1),genemap$ensembl_gene_id)
```


### Available results sets 

```{r}
    resultsNames(dds)
```


### Calculate differential expression between control and treated samples

We have previously taught RNA-seq analysis courses using the DESeq2 `results()` function to generate lists of differnetial expression

```{r}
    res1<-results(dds,contrast=c("condition","Act","Qui"))

    ## What do the results look like?
    View(res1)
    View(as.data.frame(res1))
    
    ## See a results summary
    summary(res1)
    
    ## How many genes with padj<0.05
    sum(res1$padj<0.05,na.rm=T)
    
    ## Note that some padj values are 'NA'- this is due to independent filtering
    
    ## The results parameter 'alpha' should be set to the padj cutoff that will be used for determining
    ## significant results

    res1.5<-results(dds,contrast=c("condition","Act","Qui"),alpha=0.05)

    ## How do these results differ from the previous set (res1)? Use the functions above
    
```


Recommended  method is now to use lfcShrink to generate shrunken fold changes

```{r}

    res2<-lfcShrink(dds,"condition_Act_vs_Qui",method="apeglm")

    # Look at the results for res2
    # How many genes are significant (padj<0.05)?

    # This should also work, but doesn't appear to...

    # res2.5<-lfcShrink(dds,res=res1.5,method="apeglm")

```


```{r}

    # Add annotation to results (calculated using lfcShrink)

    res2$ensembl_gene_id<-genemap$ensembl_gene_id[genemap_idx]
    res2$hgnc_symbol<-genemap$hgnc_symbol[genemap_idx]
    res2$entrezgene<-genemap$entrezgene[genemap_idx]
    res2$gene_biotype<-genemap$gene_biotype[genemap_idx]
    res2$ensembl_gene_id[which(is.na(genemap_idx))]<-
        sapply(rownames(dds),function(x) strsplit(x,".",fixed=T)[[1]][1])[which(is.na(genemap_idx))]

```


### Volcano plots

Fold change results displayed as a volcano plot. The log2 fold change is plotted on the _x_-axis, and -log10(p-value) is plotted on the _y_-axis so that the most significant differences have higher _y_-axis values.

```{r plot_volcanoes,fig.width=10,fig.height=6,eval=T}

    ## Plot points for all genes
    with(res1,plot(log2FoldChange,-log10(pvalue),xlab="log2FoldChange",
                            ylab="-log10(p-value)",main="Act vs Qui Volcano plot",pch=20,cex=0.7,col="#99999950"))
    ## Highlight genes with padj<0.05
    with(res1[which(res1$padj<0.05),],
         points(log2FoldChange,-log10(pvalue),xlab="log2FoldChange",ylab="-log10(p-value)",
                col="#EE2C2C60",pch=20,cex=0.7))

    ## Modify the above to plot the results for res2 (shrunken fold changes)    

    with(res2,plot(log2FoldChange,-log10(pvalue),xlab="log2FoldChange",
                            ylab="-log10(p-value)",main="Act vs Qui Volcano plot",pch=20,cex=0.7,col="#99999950"))
    ## Highlight genes with padj<0.05
    with(res2[which(res2$padj<0.05),],
         points(log2FoldChange,-log10(pvalue),xlab="log2FoldChange",ylab="-log10(p-value)",
                col="#EE2C2C60",pch=20,cex=0.7))

    
```
    
Another way to visualise the difference between results() and lfcShrink() is to plot the fold change for each gene calculated using the two methods.
    
```{r}        
    ## Directly compare the fold changes by both methods
    
    plot(res1$log2FoldChange,res2$log2FoldChange,xlab="log2FC with results()",ylab="log2FC with lfcShrink()",
         pch=20,cex=0.7,col="#99999950")
    ## Add lines through (0,0)
    abline(h=0,v=0)
    ## Highlight genes with padj<0.05
    points(res1[which(res1$padj<0.05),]$log2FoldChange,res2[which(res1$padj<0.05),]$log2FoldChange,
           pch=20,cex=0.7,col="#EE2C2C60")
    ## Add line x=y
    abline(a=0,b=1,lty=2)
```


```{r}
    ## Write results to file
    ## All results

    df.out<-as.data.frame(res2)
    write.table(df.out,file="Act_vs_Qui_all_genes.txt",sep="\t",row.names=F,col.names=T,quote=F)
    
    ## Significant results (padj < 0.05)
    
    write.table(df.out[which(df.out$padj<0.05),],file="Act_vs_Qui_sig_genes.txt",sep="\t",row.names=F,
                col.names=T,quote=F)
    
    ## For g.Profiler analysis, output (a) lists of Ensembl geneIDs (up or down) ranked by fold change), and 
    ## (b) a list of all Ensembl geneIDs used in the analysis
    
    # Rank df.out by log2FoldChange (low to high)
    df.out<-df.out[order(df.out$log2FoldChange),]
    sig_down<-df.out[which(df.out$padj<0.05 & df.out$log2FoldChange<0),]$ensembl_gene_id
    
    # Rank df.out high to low
    df.out<-df.out[order(df.out$log2FoldChange,decreasing=T),]
    sig_up<-df.out[which(df.out$padj<0.05 & df.out$log2FoldChange>0),]$ensembl_gene_id
    
    write.table(sig_up,file="Act_vs_Qui_up_ensembl.txt",row.names=F,col.names=F,quote=F)
    write.table(sig_down,file="Act_vs_Qui_down_ensembl.txt",row.names=F,col.names=F,quote=F)
    write.table(df.out$ensembl_gene_id,file="Act_vs_Qui_all_ensembl.txt",row.names=F,col.names=F,quote=F)
```


To run GSEA with RNA-seq data, we need a ranked list of genes. Ranking should run from up-regulated to down-regulated genes.

```{r}

    GSEA_rank<-sign(res2$log2FoldChange) * -log10(res2$pvalue)
    GSEA_rank_pval<-data.frame(hgnc_symbol=res2$hgnc_symbol,rank=GSEA_rank)

    write.table(GSEA_rank_pval[which(res2$gene_biotype=="protein_coding" & res2$hgnc_symbol!=""),],
                file="Act_vs_Qui_symbol_rank_pval.rnk",sep="\t",row.names=F,col.names=F,quote=F)    
    
    ## Could also just rank by fold change
    
    GSEA_rank_fc<-as.data.frame(res2)[,c("hgnc_symbol","log2FoldChange")]
    write.table(GSEA_rank_fc[which(res2$gene_biotype=="protein_coding" & res2$hgnc_symbol!=""),],
                file="Act_vs_Qui_symbol_rank_fc.rnk",sep="\t",row.names=F,col.names=F,quote=F)    
    
```
