# 2019 Stem Cell Network RNASeq Workshop

## Workshop Dates
October 16-18, 2019


## Workshop Locations

### Day 1 & 2
University of Ottawa Main Campus  
Lamoureux Hall (LMX)  
145 Jean-Jacques Lussier, Room 219  
[Campus Map](https://www.uottawa.ca/facilities/sites/www.uottawa.ca.facilities/files/2018facultymap.pdf)  

### Day 3 
General Hospital Campus  
501 Smyth Road, Ottawa Ontario  
Critical Care Wing  
Room W1494  

## Organizers		
Dr. Bill Stanford (uOttawa/OHRI)  
Dr. Ted Perkins (uOttawa/OHRI)  

## Workshop Files

[Workshop Agenda](2019RNA-SeqAnalysisWorkshopAgenda.pdf)

### Day 1:

[Introduction]() (Dr. Bill Stanford)

[RNA-seq library construction and sequencing]() (Dr. Bill Stanford)

[Sequencing Data Background and Review](Files/2019_Workshop_Background_and_Review.pptx)
* Example FASTQC files
    * [ERR975344.1_fastqc](https://www.ogic.ca/projects/workshop_2019/FastQC/ERR975344.1_fastqc.html?inline=false)
    * [ERR975344.2_fastqc](https://www.ogic.ca/projects/workshop_2019/FastQC/ERR975344.2_fastqc.html?inline=false)
* Example RNASEQC files
    * [Activated.1](http://www.ogic.ca/projects/workshop_2019/RNA-SeQC/Activated.1/)
    * [Quiescent.1](http://www.ogic.ca/projects/workshop_2019/RNA-SeQC/Quiescent.1/)
* Example BAM files for IGV Exercise
    * [in_day1_rep1_chr12.bam](https://gitlab.com/ohri/2019-uo-rnaseq-workshop/raw/master/Files/in_day1_rep1_chr12.bam?inline=false)
    * [in_day1_rep1_chr12.bam.bai](https://gitlab.com/ohri/2019-uo-rnaseq-workshop/raw/master/Files/in_day1_rep1_chr12.bam.bai?inline=false)
* [R package installation script](Files/install_packages.R)
* [IGV instructions](Files/IGV.md)

[Differential Gene Expression with DESeq2](Files/2019_Workshop_DGE.pptx)
* [R Script for RNASeq Data Analysis](Files/analysis_script.Rmd?inline=false)
* [Ranked significan gene list output g:Profiler exercise](https://gitlab.com/ohri/2019-uo-rnaseq-workshop/raw/master/Files/Ensembl_Act_vs_Qui_sig05_salmon.txt?inline=false)
* [Full ranked gene list ouptput for GSEA exercise](https://gitlab.com/ohri/2019-uo-rnaseq-workshop/raw/master/Files/Ensembl_Act_vs_Qui_all_salmon.txt?inline=false)

[RNA-seq Experimental Design](Files/) (Dr. Ted Perkins)

### Day 2:

[Gene Set Enrichment Analysis](https://gitlab.com/ohri/2019-uo-rnaseq-workshop/raw/master/Files/2019_Workshop_Lecture_3_-_Annotation_Enrichment.ppt)
* [g:Profiler instructions](Files/gprofiler.md)

[Single Cell RNA-seq]() (David Cook)




